-- 3.1 Modèle relationnel

-- Donner les villes que nous pouvons atteindre par vols directs qui partent de Paris
select villeA from AEROPORT where idA in (
    select aeArr from VOL where aeDep in (
        select idA from AEROPORT where villeA='Paris'
    )
);

-- En considérant les horaires des vols, veuillez fournir la liste des villes accessibles
--depuis Paris avec un vol comprenant UNE correspondance. L’objectif est de permettre aux passagers de réaliser leur correspondance
select villeA from AEROPORT where idA in (
    select v1.aeArr from VOL v1 where aeDep in (
        select v2.aeArr from VOL v2 where v1.dateDep > v2.dateArr and aeDep in (
            select idA from AEROPORT where villeA='Paris'
        )
    )
);

--En considérant les horaires des vols, veuillez fournir la liste des villes accessibles depuis Paris avec un vol comprenant DEUX correspondances
select villeA from AEROPORT where idA in (
    select v1.aeArr from VOL v1 where aeDep in (
        select v2.aeArr from VOL v2 where v1.dateDep > v2.dateArr and aeDep in (
            select v3.aeArr from VOL v3 where v2.dateDep > v3.dateArr and aeDep in (
                select idA from AEROPORT where villeA='Paris'
            )
        )
    )
);

-- Veuillez fournir la liste des villes accessibles depuis Paris, en tenant compte des 
-- horaires de vol, avec des vols directs ou un nombre quelconque de correspondances.

-- algo
i = 1
reqPrec = "select v1.aeArr from VOL v1 where aeDep in ( select idA from AEROPORT where villeA='Paris' );"
while reqPrec != null:
    i += 1
    reqPrec = "select v%s.aeArr from Vol v%s where v%s.dateDep > v%s.dateArr and aeDep in (%s)"%(i, i, i, i-1, reqPrec)
return "select villeA from AEROPORT where idA in (%s)"%(reqPrec)


--3.2 Modèle objet-relationnel

-- les objets
drop type equipTab;
drop type equipage;


-- drop type indQualite;
-- drop type indQualList;

    -- equipage
create or replace type equipage as object(
        nom varchar2(15),
        fonction varchar2(20),
        member function get_nb_pers RETURN number
);
/
create or replace type BODY equipage AS
    member function get_nb_pers RETURN number IS 
    BEGIN
        return length(self.nom);
    END;
END;
/
create or replace type equipTab as table of equipage;
/

--     -- indice de qualite
-- create or replace type indQualite as object(
--     indice varchar2(10),
--     valeur NUMBER,
--     poids NUMBER,
--     member function get_qualite RETURN NUMBER
-- );
-- /
-- create or replace type BODY indQualite AS
--     MEMBER function get_qualite RETURN NUMBER IS 
--     BEGIN
--         return self.valeur*self.poids;
--     END;
-- END;
-- /
-- create or replace type indQualList as VARRAY(10) of indQualite;
-- /


-- les tables
-- drop table VOl;
drop table COMPAGNIE;
drop table AEROPORT;
drop table TEST;

create table COMPAGNIE(
  idC NUMBER,
  nomC varchar2(20),
  PRIMARY KEY(idC)
);
create table AEROPORT(
  idA varchar2(3),
  nomA varchar2(20),
  paysA varchar2(25),
  villeA varchar2(30),
  PRIMARY KEY (idA)
);
create table TEST(
    equipage equipTab(equipage)
    -- indiceQualite indQualList
);

-- create table VOL(
--   numVol NUMBER,
--   aeDep varchar2(3),
--   dateDep DATE,
--   aeArr varchar2(3),
--   dateArr DATE,

--   idC NUMBER,
--   PRIMARY KEY(numVol),
--   FOREIGN KEY(aeDep) REFERENCES AEROPORT(idA),
--   FOREIGN KEY(aeArr) REFERENCES AEROPORT(idA),
--   FOREIGN KEY(idC) REFERENCES COMPAGNIE(idC)
-- );

-- exmples d'insert
insert into AEROPORT values('CDG', 'Charles de Gaule', 'France', 'Paris');
insert into AEROPORT values('GIG', 'Jeanne darc', 'France', 'Orleans');

-- insert into VOL values(1, 'CDG', TO_DATE('2023/03/14 10:00', 'yyyy/mm/dd hh24:mi'), 'GIG', TO_DATE('2023/03/14 12:00', 'yyyy/mm/dd hh24:mi'), equipTab(equipage('Goscinny', 'pilote'),equipage('Uderzo', 'commissaire')), indQualList(indQualite('carbone', 3, 4), indQualite('securite', 4, 5), indQualite('prix', 4, 3)), 1);
insert into TEST values(equipage('Goscinny', 'pilote'));

-- exemple de requête
select v.numVol, e.*
from VOL v, table (v.equipage) e; 
-- => numVol, nom, fonction

-- 'Vrai' requêtes
-- Pour chaque vol, donner le nombre de personnes de l’équipage, par fonction
select v.numVol, e.get_nb_pers()
from VOL v, table(v.equipage) e

-- Pour chaque pilote, indiquer combien des vols lui sont associés
select count(*)
from VOL v, table(v.equipage) e
where e.fonction = 'pilote';

-- L’impact d’un indice de qualité est donné par le produit de sa valeur et du poids
-- que lui est attribué. Pour chaque vol, indiquer l’impact de chaque indice de qualité.

-- delimiter |
-- BEGIN
--     DECLARE carb 
--     DECLARE fini boolean default false;
--     DECLARE impacts cursor for
--         select v.indiceQualite  from VOL v;
--     DECLARE continu handler for not found set fini = true;
--     open impacts;
--     while not fini do
--         fetch impacts into carb, sec, prix
-- END |
-- delimiter;