drop table VOl;
drop table COMPAGNIE;
drop table AEROPORT;

create table COMPAGNIE(
  idC NUMBER,
  nomC varchar2(20),
  PRIMARY KEY(idC)
);

create table AEROPORT(
  idA NUMBER,
  nomA varchar2(20),
  paysA varchar2(25),
  villeA varchar2(30),
  PRIMARY KEY (idA)
);

create table VOL(
  numVol NUMBER,
  dateDep DATE,
  dateArr DATE,
  aeDep NUMBER,
  aeArr NUMBER,
  terminalDep varchar2(30),
  terminalArr varchar2(30),
  idC NUMBER,
  PRIMARY KEY(numVol),
  FOREIGN KEY(aeDep) REFERENCES AEROPORT(idA),
  FOREIGN KEY(aeArr) REFERENCES AEROPORT(idA),
  FOREIGN KEY(idC) REFERENCES COMPAGNIE(idC)
);

insert into COMPAGNIE values (1, 'AGSAO');
insert into AEROPORT values(1, 'Charles de Gaule', 'France', 'Paris');
insert into AEROPORT values(2, 'Jeanne darc', 'France', 'Orleans');
insert into AEROPORT values(3, 'Le Bordelais', 'France', 'Bordeaux');
insert into AEROPORT values(4, 'Le Jaune', 'France', 'Marseille');
insert into AEROPORT values(5, 'Les Anges', 'Etats-Unis', 'Los Angeles');
insert into VOL values(1, TO_DATE('2023/03/14 10:00', 'yyyy/mm/dd hh24:mi'), TO_DATE('2023/03/14 12:00', 'yyyy/mm/dd hh24:mi'), 1, 2, 'Terminal 2F', 'Terminal 1', 1);
insert into VOL values(2, TO_DATE('2023/03/18 12:00', 'yyyy/mm/dd hh24:mi'), TO_DATE('2023/03/18 16:00', 'yyyy/mm/dd hh24:mi'), 1, 3, 'Terminal 3A', 'Terminal 2', 1);
insert into VOL values(3, TO_DATE('2023/03/22 10:00', 'yyyy/mm/dd hh24:mi'), TO_DATE('2023/03/22 13:00', 'yyyy/mm/dd hh24:mi'), 1, 4, 'Terminal 1C', 'Terminal 4', 1);
insert into VOL values(4, TO_DATE('2023/03/20 5:00', 'yyyy/mm/dd hh24:mi'), TO_DATE('2023/03/20 12:00', 'yyyy/mm/dd hh24:mi'), 1, 5, 'Terminal 2A', 'Terminal CC', 1);

-- Une correspondance
insert into VOL values(5, TO_DATE('2023/03/14 12:30', 'yyyy/mm/dd hh24:mi'), TO_DATE('2023/03/18 15:00', 'yyyy/mm/dd hh24:mi'), 2, 3, 'Terminal 2', 'Terminal 3', 1);

-- Une et Deux correspondances
insert into VOL values(6, TO_DATE('2023/03/18 16:30', 'yyyy/mm/dd hh24:mi'), TO_DATE('2023/03/18 20:00', 'yyyy/mm/dd hh24:mi'), 3, 5, 'Terminal 3', 'Terminal DD', 1);